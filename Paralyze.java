import javax.swing.*;
import java.awt.*;
public class Paralyze extends JFrame
{
	public Paralyze(Rectangle bounds)
	{
		setUndecorated(true);
		setBackground(new Color(0, 0, 0, 0));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		setOpacity(0.1f);
		add(new ParalyzePanel());
		setLocation(bounds.getLocation());
		setSize(bounds.getSize());
		setAlwaysOnTop(true);
		setVisible(true);
	}
}