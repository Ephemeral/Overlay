import java.awt.*;
import javax.swing.*;
public class SelectionFrame extends JFrame implements Runnable
{
	Overlay overlay;
	public SelectionFrame(Overlay overlay)
	{
		this.overlay = overlay;
		run();
	}
	public void run()
	{
		try 
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} 
		catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) 
		{

		}

		setUndecorated(true);
		setBackground(new Color(0, 0, 0, 0));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		add(new DraggablePanel(this));
		Rectangle bounds = getVirtualBounds();
		setLocation(bounds.getLocation());
		setSize(bounds.getSize());
		setAlwaysOnTop(true);
		setVisible(true);
	}
	public void destroyObject(Rectangle bar)
	{
		dispose();
		overlay.buildOverlayFrame(bar);
	}
	public Rectangle getVirtualBounds() 
	{
		Rectangle bounds = new Rectangle(0, 0, 0, 0);

		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice lstGDs[] = ge.getScreenDevices();
		for (GraphicsDevice gd : lstGDs) 
		{
			bounds.add(gd.getDefaultConfiguration().getBounds());
		}
		return bounds;
	}
}