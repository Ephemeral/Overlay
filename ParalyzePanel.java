import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.script.*;
public class ParalyzePanel extends JPanel
{
	public ParalyzePanel()
	{
		MouseAdapter mouseHandler = new MouseAdapter() 
		{
			@Override
			public void mouseReleased(MouseEvent e) 
			{
				final String script="tell application \"System Events\"\n" +
						"\tname of application processes whose frontmost is true\n" +
						"end";
				ScriptEngine appleScript=new ScriptEngineManager().getEngineByName("AppleScript");
				String result = "";
				try
				{
					result=(String)appleScript.eval(script);
				}
				
				catch(ScriptException scriptexception)
				{
					
				}
				System.out.println(result);
			}
		};
		addMouseListener(mouseHandler);
	}

}