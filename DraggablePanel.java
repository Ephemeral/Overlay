import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
public class DraggablePanel extends JPanel
{
	private Rectangle selectionBounds;
	private Point clickPoint;
	private DraggablePanel self;
	public DraggablePanel(SelectionFrame form)
	{
		self = this;
		setOpaque(false);
		MouseAdapter mouseHandler = new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) 
				{
					
				}
			}

			@Override
			public void mousePressed(MouseEvent e)
			 {

				clickPoint = e.getPoint();
				selectionBounds = null;
			}

			@Override
			public void mouseReleased(MouseEvent e) 
			{

				clickPoint = null;
				form.destroyObject(selectionBounds);
			}

			@Override
			public void mouseDragged(MouseEvent e) 
			{
				Point dragPoint = e.getPoint();
				int x = Math.min(clickPoint.x, dragPoint.x);
				int y = Math.min(clickPoint.y, dragPoint.y);
				int width = Math.max(clickPoint.x - dragPoint.x, dragPoint.x - clickPoint.x);
				int height = Math.max(clickPoint.y - dragPoint.y, dragPoint.y - clickPoint.y);
				selectionBounds = new Rectangle(x, y, width, height);
				repaint();
			}
		};

		addMouseListener(mouseHandler);
		addMouseMotionListener(mouseHandler);
	}
	@Override

	protected void paintComponent(Graphics g) 
	{
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g.create();
		g2d.setColor(new Color(255, 255, 255, 128));

		Area fill = new Area(new Rectangle(new Point(0, 0), getSize()));
		if (selectionBounds != null) {
			fill.subtract(new Area(selectionBounds));
		}
		g2d.fill(fill);
		if (selectionBounds != null) {
			g2d.setColor(Color.BLACK);
			g2d.draw(selectionBounds);
		}
		g2d.dispose();
	}
}